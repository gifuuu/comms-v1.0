/*
Login Functions:
    validateLoginForm()
    validateUserLogin()
    getLoginSession()
    setLoginSession()
    checkLoginSession()
*/

function validateLoginForm() {
  var emailVal = document.getElementById("email").value;
  var passwordVal = document.getElementById("password").value;

  var atPosition = emailVal.indexOf("@");
  var dotPosition = emailVal.lastIndexOf(".");

  if (emailVal == "") {
    alert("Please enter your email");
    return false;
  } else if (atPosition < 1 || dotPosition - atPosition < 2) {
    alert("Please enter valid email");
    return false;
  } else if (passwordVal == "") {
    alert("Please enter password");
    return false;
  } else if (passwordVal.length < 3) {
    alert("Password length must be atleast 3 characters");
    return false;
  }
  return true;
}

function validateUserLogin() {
  var users = populateUsers();
  var emailVal = document.getElementById("email").value;
  var passwordVal = document.getElementById("password").value;

  var user = users.find((user) => {
    return user.email === emailVal && user.password === passwordVal;
  });

  if (!user) {
    alert("Please ensure your email and password is correct");
    return false;
  } else {
    localStorage.setItem("activeUser", JSON.stringify(user));
    setLoginSession("userId", user.userId, 30);
    return true;
  }
}

function getLoginSession(cname) {
  if (document.cookie.length > 0) {
    cookie =
      document.cookie.match("(^|;)\\s*" + cname + "\\s*=\\s*([^;]+)")?.pop() ||
      "";
    return cookie;
  }
  return "";
}

function setLoginSession(cname, cvalue, exdays) {
  const d = new Date();
  d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
  let expires = "expires=" + d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function checkLoginSession() {
  var userId = getLoginSession("userId");
  if (userId !== null && userId !== "") {
    return userId;
  }
  return "N/A";
}

function deleteCookie(name) {
  setLoginSession(name, null, -1);
}

function postUser() {
  var userid = checkLoginSession();
  console.log(userid);
  var user = getUserInfo(userid);
  document.querySelector("#welcomeMessage").innerHTML =
    "<b>Welcome !</b>" + " " + user.email;
}

function logout() {
  deleteCookie("userId");
}

function viewUser(id) {
  var users = populateUsers();

  for (user = 0; user < users.length; user++) {
    if (users[user].userId == id) {
      document.getElementById("fullName").value = users[user].fullName;
      document.getElementById("email").value = users[user].email;
    }
  }
}

function viewUploadInfo(id) {
  var uploads = populateUploads();

  for (var upload = 0; upload < uploads.length; upload++) {
    if (uploads[upload].uploadId == id) {
      document.getElementById("editLabelInput").value = uploads[upload].label;
    }
  }
}

function validateUpdateForm() {
  var fullNameVal = document.getElementById("fullName").value;
  var emailVal = document.getElementById("email").value;

  var atPosition = emailVal.indexOf("@");
  var dotPosition = emailVal.lastIndexOf(".");

  if (fullNameVal == "") {
    alert("Please enter full name");
    return false;
  } else if (emailVal == "") {
    alert("Please enter your email");
    return false;
  } else if (atPosition < 1 || dotPosition - atPosition < 2) {
    alert("Please enter valid email");
    return false;
  }

  return true;
}

// will get 1 user info properties only
function getUserInfo(id) {
  var users = populateUsers();
  var user;
  for (i = 0; i < users.length; i++) {
    if (users[i].userId == id) {
      user = users[i];
      return user;
    }
  }
}

function updateUserInfo(id, newName, newEmail) {
  if (id !== "" && newName !== "" && newEmail !== "") {
    var users = populateUsers();
    for (i = 0; i < users.length; i++) {
      if (users[i].userId == id) {
        users[i].fullName = newName;
        users[i].email = newEmail;
        localStorage.setItem("users", JSON.stringify(users));
        return true;
      }
    }
  } else {
    return false;
  }
}

function updateUploadInfo(id, newLabel) {
  if (newLabel !== "") {
    var uploads = populateUploads();

    for (var i = 0; i < uploads.length; i++) {
      if (uploads[i].uploadId == id) {
        uploads[i].label = newLabel;
        localStorage.setItem("uploads", JSON.stringify(uploads));

        return true;
      }
    }
  } else {
    alert("Please add label");
    return false;
  }
}

function register() {
  //Inits
  var id = Date.now();
  var fullName = document.getElementById("fullName").value;
  var email = document.getElementById("email").value;
  var password = document.getElementById("password").value;
  var users = [];

  var user = {
    userId: id,
    fullName: fullName,
    email: email,
    password: password,
  };

  if (!localStorage.getItem("users")) {
    // If there is no users it will add an item
    users.push(user);
    localStorage.setItem("users", JSON.stringify(users));
  } else {
    //else it will push an object in an users array
    users = JSON.parse(localStorage.getItem("users"));
    users.push(user);
    localStorage.setItem("users", JSON.stringify(users));
  }

  return true;
}

function populateUsers() {
  var users = [];
  users = JSON.parse(localStorage.getItem("users"));

  return users;
}

function displayUsersTable() {
  var users = populateUsers();
  for (user = 0; user < users.length; user++) {
    var id = users[user].userId;
    var name = users[user].fullName;
    var email = users[user].email;
    const table = document
      .getElementById("usersTable")
      .getElementsByTagName("tbody")[0];
    var row = table.insertRow();
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    var newName = document.createTextNode(name);
    var newEmail = document.createTextNode(email);

    var actions = `<a class='noDeco' href='./edit.html?id=${id}'>Edit</a> | <a class='noDeco' href='#' onclick='deleteUser("${id}"); return false;'>Delete</a>`;

    cell1.appendChild(newName);
    cell2.appendChild(newEmail);
    cell3.innerHTML += actions;
    cell2.className = "alignCenter";
    cell3.className = "alignCenter";
  }
}

function displayMyUploadsTable() {
  var uploads = populateUploads(1);
  for (upload = 0; upload < uploads.length; upload++) {
    var uploadId = uploads[upload].uploadId;
    var label = uploads[upload].label;
    var fileName = uploads[upload].fileName;

    const table = document
      .getElementById("myUploadsTable")
      .getElementsByTagName("tbody")[0];
    var row = table.insertRow();
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    var newLabel = document.createTextNode(label);
    var newFilename = document.createTextNode(fileName);

    var actions = `<a class='noDeco' href='#' onclick='editUploadModal("${uploadId}"); return false;'>Edit</a> | <a class='noDeco' href='#' onclick='deleteUploadModal("${uploadId}"); return false;'>Delete</a> |  <a class='noDeco' href='./share.html?uploadId=${uploadId}&name=${fileName}'>Share</a>`;

    cell1.appendChild(newLabel);
    cell2.appendChild(newFilename);
    cell3.innerHTML += actions;
    cell2.className = "alignCenter";
    cell3.className = "alignCenter";
  }
}

function displaySharedUsersTable(id) {
  var sharedUsers = populateSharedUsers(id);

  for (user = 0; user < sharedUsers.length; user++) {
    var userId = sharedUsers[user];
    var name = getUserInfo(userId);
    const table = document
      .querySelector("#sharedUserTable")
      .getElementsByTagName("tbody")[0];
    var row = table.insertRow();
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var newUser = document.createTextNode(name.fullName);

    var action = `<a class='noDeco' href='#' onclick='deleteSharedUserModal("${id}", " ${userId}"); return false;'>Remove</a>`;

    cell1.appendChild(newUser);
    cell2.innerHTML = action;
    cell2.className = "alignCenter";
  }
}

function displaySharedFilesTable() {
  var sharedFiles = populateSharedFiles();

  for (file = 0; file < sharedFiles.length; file++) {
    var label = sharedFiles[file].label;
    var fileName = sharedFiles[file].fileName;
    var author = getUserInfo(sharedFiles[file].userId);
    const table = document
      .querySelector("#sharedUploadsTable")
      .getElementsByTagName("tbody")[0];
    var row = table.insertRow();
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    var newLabel = document.createTextNode(label);
    var newFileName = document.createTextNode(fileName);
    var newAuthor = document.createTextNode(author.fullName);

    cell1.appendChild(newLabel);
    cell2.appendChild(newFileName);
    cell2.className = "alignCenter";
    cell3.appendChild(newAuthor);
    cell3.className = "alignCenter";
  }
}

function validateRegistrationForm() {
  var fullName = document.getElementById("fullName").value;
  var email = document.getElementById("email").value;
  var password = document.getElementById("password").value;
  var confirmPassword = document.getElementById("confirmPassword").value;

  var atPosition = email.indexOf("@");
  var dotPosition = email.lastIndexOf(".");

  if (fullName == "") {
    alert("Please enter your full name");
    return false;
  } else if (email == "") {
    alert("Please enter your email");
    return false;
  } else if (atPosition < 1 || dotPosition - atPosition < 2) {
    alert("Please enter valid email");
    return false;
  } else if (password == "") {
    alert("Please enter password");
    return false;
  } else if (password.length < 3) {
    alert("Password length must be atleast 3 characters");
    return false;
  } else if (confirmPassword == "") {
    alert("Please enter confirmation password");
    return false;
  } else if (password !== confirmPassword) {
    alert("Please ensure your password is same");
    return false;
  }
  return true;
}

function uploadFile() {
  var upload = {};
  var uploads = [];

  var uploadId = Date.now();
  var label = document.getElementById("labelInput");
  var user = JSON.parse(localStorage.getItem("activeUser"));
  var fileName = document.getElementById("fileName").innerHTML;

  upload.uploadId = uploadId;
  upload.label = label.value;
  upload.userId = user.userId;
  upload.fileName = fileName;
  upload.sharedTo = [];

  if (!localStorage.getItem("uploads")) {
    // If there is no users it will add an item
    uploads.push(upload);
    localStorage.setItem("uploads", JSON.stringify(uploads));
    return true;
  } else {
    //else it will push an object in an users array
    uploads = JSON.parse(localStorage.getItem("uploads"));
    uploads.push(upload);
    localStorage.setItem("uploads", JSON.stringify(uploads));
    return true;
  }
}

function shareFile(uploadId, selectedUser) {
  var uploads = populateUploads();
  var upload = populateUploads().find((upload) => {
    return upload.uploadId == uploadId;
  });

  //checking
  var userShared = upload.sharedTo.find((userShared) => {
    return userShared == selectedUser;
  });

  if (!userShared) {
    upload.sharedTo.push(selectedUser);
    for (let i = 0; i < uploads.length; i++) {
      if (uploads[i].uploadId == upload.uploadId) {
        console.log("upload matched");
        uploads[i] = upload;
        console.log(uploads[i]);
        localStorage.setItem("uploads", JSON.stringify(uploads));
        return true;
      }
    }
  } else {
    alert("User already shared");
  }
}

// will return filtered
function populateUploads(filtered) {
  var uploads = JSON.parse(localStorage.getItem("uploads"));

  if (filtered == 1) {
    var activeUser = getLoginSession("userId");
    var filteredUploads = uploads.filter(function (userUploads) {
      return userUploads.userId == activeUser;
    });
    return filteredUploads;
  }
  return uploads;
}

function populateSharedUsers(uploadId) {
  var uploads = JSON.parse(localStorage.getItem("uploads"));

  var upload = uploads.find((upload) => {
    return upload.uploadId == uploadId;
  });
  return upload.sharedTo;
}

function populateSharedFiles() {
  var sharedFiles = [];
  var uploads = JSON.parse(localStorage.getItem("uploads"));
  var activeUser = getLoginSession("userId");
  for (let i = 0; i < uploads.length; i++) {
    for (let j = 0; j < uploads[i].sharedTo.length; j++) {
      if (uploads[i].sharedTo[j] == activeUser) {
        sharedFiles.push(uploads[i]);
      }
    }
  }
  return sharedFiles;
}

// Modal Functions
function deleteConfirmation() {
  var modal = document.querySelector("#deleteModal");
  var span = document.querySelector("#closeEdit");

  modal.style.display = "block";

  span.onclick = function () {
    modal.style.display = "none";
  };
}

function deleteUser(id) {
  var modal = document.querySelector("#deleteUserModal");
  var span = document.querySelector("#closeDeleteUser");
  var btnOk = document.querySelector("#btnOk");
  modal.style.display = "block";

  span.onclick = function () {
    modal.style.display = "none";
  };

  btnOk.onclick = function () {
    // For Segregation and clean code will add to new function
    var users = populateUsers();
    for (i = 0; i < users.length; i++) {
      if (users[i].userId == id) {
        console.log(users[i]);
        users.splice(i, 1);
        localStorage.setItem("users", JSON.stringify(users));
      }
    }
  };
}

function deleteSharedUser(uploadId, targetUser) {
  var user = targetUser.replace(/ /g, ""); //trimming if there is whitespace

  var upload = populateUploads().find((upload) => {
    return upload.uploadId == uploadId;
  });

  for (let i = 0; i < upload.sharedTo.length; i++) {
    console.log("loop " + i);
    if (upload.sharedTo[i] == user) {
      console.log("found " + i);
      upload.sharedTo.splice(i, 1);
      console.log(i + "  " + upload.sharedTo[i]);
    }
  }

  var uploads = populateUploads();

  for (let i = 0; i < uploads.length; i++) {
    if (uploads[i].uploadId == upload.uploadId) {
      console.log("found on uploads " + i);
      uploads[i] = upload;
      localStorage.setItem("uploads", JSON.stringify(uploads));
    }
  }
}

function deleteSharedUserModal(uploadId, targetUser) {
  var modal = document.querySelector("#deleteSharedUserModal");
  var span = document.querySelector("#closeDeleteSharedUser");
  var btnConfirm = document.querySelector("#btnDeleteSharedUser");
  var btnCancel = document.querySelector("#btnCancelDeleteSharedUser");
  modal.style.display = "block";

  btnConfirm.onclick = function () {
    deleteSharedUser(uploadId, targetUser);
    modal.style.display = "none";
    location.reload();
  };

  btnCancel.onclick = function () {
    modal.style.display = "none";
  };

  span.onclick = function () {
    modal.style.display = "none";
  };
}

function deleteUploadModal(id) {
  var modal = document.querySelector("#deleteModal");
  var span = document.querySelector("#closeDelete");
  var btnOk = document.querySelector("#btnOk");
  modal.style.display = "block";

  span.onclick = function () {
    modal.style.display = "none";
  };
  btnOk.onclick = function () {
    modal.style.display = "none";
    deleteUpload(id);
  };
}

function editUploadModal(id) {
  viewUploadInfo(id);
  var modal = document.querySelector("#editModal");
  var span = document.querySelector("#closeEdit");
  var btnUpdate = document.querySelector("#btnUpdate");
  var btnCancel = document.querySelector("#btnCancelUpdate");
  var newLabel = document.querySelector("#editLabelInput");
  modal.style.display = "block";

  span.onclick = function () {
    modal.style.display = "none";
  };

  btnCancel.onclick = function () {
    modal.style.display = "none";
  };

  btnUpdate.onclick = function () {
    modal.style.display = "none";
    if (newLabel.value !== "") {
      updateUploadInfo(id, newLabel.value);
    }
  };
}

function deleteUpload(id) {
  var uploads = populateUploads();
  for (i = 0; i < uploads.length; i++) {
    if (uploads[i].uploadId == id) {
      console.log(uploads[i]);
      uploads.splice(i, 1);
      localStorage.setItem("uploads", JSON.stringify(uploads));
    }
  }
}

function uploadModal() {
  var modal = document.querySelector("#uploadModal");
  var span = document.querySelector("#closeUpload");
  var btnSave = document.querySelector("#save");
  var realBtn = document.querySelector("#realButton");
  var btnChooseFile = document.querySelector("#chooseFile");
  var label = document.querySelector("#labelInput");
  var fileName = document.querySelector("#fileName");
  modal.style.display = "block";
  var fN = fileName.innerHTML;
  span.onclick = function () {
    modal.style.display = "none";
  };

  btnChooseFile.onclick = function () {
    modal.style.display = "block";
    realBtn.click();
  };

  realBtn.onchange = function () {
    if (realBtn.value) {
      modal.style.display = "block";
      fileName.innerHTML = realBtn.value.match(/[\/\\]([\w\d\s\.\-\(\)]+)$/)[1];
    }
  };

  btnSave.onclick = function () {
    var fN = fileName.innerHTML;
    if (label.value !== "" && fN !== "") {
      uploadFile();
      modal.style.display = "none";
      location.reload();
      return true;
    } else {
      label.style.border = "2px solid #FF0000";
      btnChooseFile.style.border = "2px solid #FF0000";
      return false;
    }
  };
}

//populating users inside for the dropdown

function dropdownUsers() {
  let dropdown = document.querySelector("#usersDropdown");
  let users = populateUsers();
  var activeUserId = getLoginSession("userId");

  console.log(activeUserId);
  for (let user = 0; user < users.length; user++) {
    if (users[user].userId != activeUserId) {
      console.log(users[user].userId);
      let option = document.createElement("option");
      option.value = users[user].userId;
      option.innerHTML = users[user].fullName;
      dropdown.appendChild(option);
    }
  }
}

function formatDate(date) {
  let dayOfMonth = date.getDate();
  let month = date.getMonth() + 1;
  let year = date.getFullYear();
  let hour = date.getHours();
  let minutes = date.getMinutes();
  let seconds = date.getSeconds();

  // formatting

  month = month < 10 ? "0" + month : month;
  dayOfMonth = dayOfMonth < 10 ? "0" + dayOfMonth : dayOfMonth;
  hour = hour < 10 ? "0" + hour : hour;
  minutes = minutes < 10 ? "0" + minutes : minutes;

  return (
    year +
    "-" +
    month +
    "-" +
    dayOfMonth +
    " " +
    hour +
    ":" +
    minutes +
    ":" +
    seconds
  );
}

function sendMessage(message) {
  var userId = getLoginSession("userId");
  var today = new Date();
  var chats = [];
  var chat = {
    chatId: Date.now(),
    sender: userId,
    message: message,
    date: formatDate(today),
  };

  if (!populateChats()) {
    chats.push(chat);
    localStorage.setItem("chats", JSON.stringify(chats));
    return true;
  } else {
    chats = populateChats();
    chats.push(chat);
    localStorage.setItem("chats", JSON.stringify(chats));
    return true;
  }
}

function populateChats() {
  var chats = JSON.parse(localStorage.getItem("chats"));
  return chats;
}

// on a bucket
function displayChats() {
  var chats = populateChats();

  for (let i = 0; i < chats.length; i++) {
    var messages = document.querySelector("#messages");

    var messageBody = chats[i].message;
    var sender = getUserInfo(chats[i].sender).fullName;
    var dateSent = chats[i].date;

    var messageElement = document.createElement("p");

    messageElement.innerHTML =
      "[" + dateSent + "] " + sender + ": " + messageBody;

    messageElement.className = "msg-el";

    messages.appendChild(messageElement);
  }
}
